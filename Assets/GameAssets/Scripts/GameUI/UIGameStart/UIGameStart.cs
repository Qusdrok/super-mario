using System.Collections.Generic;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameManager;
using GameAssets.Scripts.GamePlay;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameUI.UIGameStart
{
    public partial class UIGameStart : BaseUI
    {
        [SerializeField] private GameObject levelPrefab;
        [SerializeField] private GameObject pagePrefab;
        [SerializeField] private Transform pageParent;

        [SerializeField] private Button btnExit;
        [SerializeField] private Button btnNextPage;
        [SerializeField] private Button btnPrevPage;

        private readonly List<Transform> _pages = new List<Transform>();

        private int _totalPage = -1;
        private int _indexCurrentPage;

        private void CreateLevel(int index)
        {
            var level = Instantiate(levelPrefab, _pages[_totalPage]);
            level.name = $"Level {index}";
            level.GetComponent<Level>().Init(index + 1, UserDataManager.Instance.UnlockLevel(index));
        }

        private void CreatePage()
        {
            _totalPage++;
            var page = Instantiate(pagePrefab, pageParent);
            page.name = $"Page {_totalPage}";
            _pages.Add(page.transform);
        }
    }
}