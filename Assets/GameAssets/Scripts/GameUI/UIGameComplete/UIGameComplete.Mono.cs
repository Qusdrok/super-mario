using GameAssets.Scripts.GameManager;

namespace GameAssets.Scripts.GameUI.UIGameComplete
{
    public partial class UIGameComplete
    {
        private void Awake()
        {
            btnGameComplete.onClick.AddListener(SceneManager.ResetCurrentScene);
            btnExit.onClick.AddListener(() => UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("Game Menu"));
            
            btnNextLevel.onClick.AddListener(() =>
            {
                UserDataManager.Instance.SelectLevel(UserDataManager.Instance.userDataSave.selectLevel++);
                SceneManager.ResetCurrentScene();
            });
        }
    }
}