using DG.Tweening;
using GameAssets.Scripts.GameManager;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GameUI.UILoading
{
    public partial class UILoading
    {
        private void Awake()
        {
            var loop = (int) (loadingTime / loadingDuration) % 2 == 0
                ? (int) (loadingTime / loadingDuration)
                : (int) (loadingTime / loadingDuration) + 1;
            imgLoading.transform.DORotate(Vector3.forward * -180f, loadingDuration)
                .SetEase(Ease.Linear)
                .SetLoops(loop, LoopType.Incremental)
                .OnComplete(() => UIManager.Instance.HideAllAndShowUI(UIType.UIPlay));
        }
    }
}