﻿using GameAssets.Scripts.GameManager;
using GameAssets.Scripts.General;
using SceneManager = UnityEngine.SceneManagement.SceneManager;

namespace GameAssets.Scripts.GameUI.UIPause
{
    public partial class UIPause
    {
        private void Awake()
        {
            btnHome.onClick.AddListener(() => SceneManager.LoadSceneAsync("Game Menu"));
            
            btnResume.onClick.AddListener(() =>
            {
                GameManager.GameManager.Instance.SetGameState(GameState.Playing);
                gameObject.SetActive(false);
                UIManager.Instance.ChangeSpritePause();
            });
            
            btnRestart.onClick.AddListener(GameManager.SceneManager.ResetCurrentScene);
        }
    }
}