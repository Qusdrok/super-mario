﻿using GameAssets.Scripts.GameBase;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameUI.UIPause
{
    public partial class UIPause : BaseUI
    {
        [SerializeField] private Button btnHome;
        [SerializeField] private Button btnResume;
        [SerializeField] private Button btnRestart;
    }
}