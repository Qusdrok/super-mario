using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameManager;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GameUI.UIPlay
{
    public partial class UIPlay
    {
        private static readonly int Start = Animator.StringToHash("Start");

        private void Awake()
        {
            if (btnPlay != null)
            {
                btnPlay.onClick.AddListener(() => UIManager.Instance.ShowUI<UIGameStart.UIGameStart>());
            }

            if (btnPause != null)
            {
                btnPause.onClick.AddListener(() =>
                {
                    GameManager.GameManager.Instance.SetGameState(GameState.Pause);
                    UIManager.Instance.ShowUI<UIPause.UIPause>();
                    UIManager.Instance.ChangeSpritePause();
                });
            }

            if (btnShoot == null)
            {
                return;
            }

            startAnimator.SetBool(Start, true);
            startAnimator.OnComplete(() => GameManager.GameManager.Instance.SetGameState(GameState.Playing));

            btnJump.onClick.AddListener(GameEvent.GameEvent.DoPlayerJump);
            btnShoot.onClick.AddListener(GameEvent.GameEvent.DoPlayerShoot);
        }
    }
}