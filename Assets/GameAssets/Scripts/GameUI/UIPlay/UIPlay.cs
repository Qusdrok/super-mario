using GameAssets.Scripts.GameBase;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameUI.UIPlay
{
    public partial class UIPlay : BaseUI
    {
        [SerializeField] private Animator startAnimator;
        [SerializeField] private Button btnPlay;
        [SerializeField] private Button btnPause;
        [SerializeField] private Button btnShoot;
        [SerializeField] private Button btnJump;
    }
}