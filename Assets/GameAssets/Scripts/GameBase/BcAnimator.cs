using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GameBase
{
    public enum AnimationType
    {
        Idle,
        Attack,
        Run,
        Move,
        Death,
        Jump,
        Falling
    }

    [RequireComponent(typeof(Animator))]
    public class BcAnimator : MonoBehaviour
    {
        [SerializeField] private AnimationType currentAnimation;
        [SerializeField] private Animator animator;

        [Button]
        private void Initiation()
        {
            animator = GetComponent<Animator>();
        }

        public bool IsAnimation(AnimationType type) => currentAnimation.Equals(type);

        public void ChangeAnimation(AnimationType pa)
        {
            if (pa.Equals(currentAnimation))
            {
                return;
            }
            
            currentAnimation = pa;
            animator.SetTrigger(pa.ToString());
        }

        public void ChangeAnimatorLayer(string layerName, bool active = true)
        {
            for (var i = 0; i < animator.layerCount; i++)
            {
                animator.SetLayerWeight(i, 0);
            }

            animator.SetLayerWeight(animator.GetLayerIndex(layerName), active ? 1 : 0);
        }
    }
}