﻿using GameAssets.Scripts.GameBase.Interface;

namespace GameAssets.Scripts.GameBase
{
    public class BaseCommand : ICommand
    {
        public virtual void Execute()
        {
        }

        public virtual void Undo()
        {
        }

        public virtual bool IsFinished()
        {
            return false;
        }
    }
}