﻿using GameAssets.Scripts.GameBase.Interface;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.General;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GameBase
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(BcAnimator))]
    [RequireComponent(typeof(BaseStat))]
    public abstract class BaseCharacter : MonoBehaviour
    {
        [FoldoutGroup("Initiation")] [ReadOnly]
        public BcAnimator bcAnimator;

        [FoldoutGroup("Initiation")] [ReadOnly]
        public new Collider2D collider;

        [FoldoutGroup("Initiation")] [ReadOnly]
        public IState currentState;

        [FoldoutGroup("Initiation")] [ReadOnly]
        public Rigidbody2D rb;

        [FoldoutGroup("Initiation")] [ReadOnly]
        public BaseStat stat;

        [FoldoutGroup("Initiation")] [ReadOnly] [SerializeField]
        private string stateName;

        [Button]
        protected virtual void Initiation()
        {
            bcAnimator = GetComponent<BcAnimator>();
            collider = GetComponent<Collider2D>();
            rb = GetComponent<Rigidbody2D>();
            stat = GetComponent<BaseStat>();
        }

        private void OnValidate()
        {
            Initiation();
        }

        private void Update()
        {
            if (!GameManager.GameManager.Instance.IsGameState(GameState.Playing) || !stat.IsAlive)
            {
                return;
            }

            InnerUpdate();
        }

        private void FixedUpdate()
        {
            if (!GameManager.GameManager.Instance.IsGameState(GameState.Playing) || !stat.IsAlive)
            {
                return;
            }

            InnerFixedUpdate();
        }

        private void LateUpdate()
        {
            if (!GameManager.GameManager.Instance.IsGameState(GameState.Playing) || !stat.IsAlive)
            {
                return;
            }

            InnerLateUpdate();
        }

        protected virtual void InnerUpdate()
        {
        }

        protected virtual void InnerFixedUpdate()
        {
        }

        protected virtual void InnerLateUpdate()
        {
        }

        public void ChangeState(IState state = null, MonoBehaviour target = null)
        {
            stateName = $"{state?.ToString().Match('.', isFirstLast: true)}";
            currentState = state;
            currentState?.ExitState();

            if (rb != null)
            {
                rb.ResetInertia();
            }

            if (state == null)
            {
                stat.ChangeCharacterState(CharacterState.IdleOrRespawn);
                return;
            }

            /*Debug.Log($"{name} STATE ===> {newState}");*/
            currentState.Character = this;
            currentState.StartState(target);
        }
    }
}