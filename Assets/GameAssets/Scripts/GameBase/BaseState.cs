﻿using GameAssets.Scripts.GameBase.Interface;
using UnityEngine;

namespace GameAssets.Scripts.GameBase
{
    public abstract class BaseState : IState
    {
        public BaseCharacter Character { get; set; }
        public MonoBehaviour Target { get; set; }

        private bool _isCompleted;
        private bool _isFrameCompleted;

        public void StartState(MonoBehaviour target = null)
        {
            if (target != null)
            {
                Target = target;
            }

            if (!Character.stat.IsAlive || !GameManager.GameManager.Instance.IsGameState(General.GameState.Playing))
            {
                return;
            }

            InnerStartState();
        }

        protected virtual void InnerStartState()
        {
        }

        public void UpdateState()
        {
            if (_isCompleted || _isFrameCompleted || !Character.stat.IsAlive ||
                !GameManager.GameManager.Instance.IsGameState(General.GameState.Playing))
            {
                return;
            }

            InnerUpdateState();
        }

        protected virtual void InnerUpdateState()
        {
        }

        public virtual void ExitState()
        {
        }

        protected void SetIsComplete(bool completed)
        {
            _isCompleted = completed;
        }

        protected void SetIsFrameCompleted(bool frameCompleted)
        {
            _isFrameCompleted = frameCompleted;
        }

        protected void OnCompleted()
        {
            if (!_isCompleted || !Character.stat.IsAlive ||
                !GameManager.GameManager.Instance.IsGameState(General.GameState.Playing))
            {
                return;
            }

            SetIsComplete(false);
            InnerOnComplete();
        }

        protected virtual void InnerOnComplete()
        {
        }

        protected void OnFrameCompleted()
        {
            if (!_isFrameCompleted || !Character.stat.IsAlive ||
                !GameManager.GameManager.Instance.IsGameState(General.GameState.Playing))
            {
                return;
            }

            SetIsFrameCompleted(false);
            InnerOnFrameCompleted();
        }

        protected virtual void InnerOnFrameCompleted()
        {
        }
    }
}