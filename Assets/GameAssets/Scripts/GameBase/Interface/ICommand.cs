﻿namespace GameAssets.Scripts.GameBase.Interface
{
    public interface ICommand
    {
        void Execute();
        void Undo();
        bool IsFinished();
    }
}
