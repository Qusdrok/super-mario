﻿using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GameBase
{
    public class BaseUI : MonoBehaviour
    {
        [SerializeField] private UIType uiType;

        public UIType UIType => uiType;

        public void DoAnimation(bool active)
        {
            if (active)
            {
                if (gameObject.activeInHierarchy)
                {
                    return;
                }

                gameObject.SetActive(true);
            }
            else
            {
                if (!gameObject.activeInHierarchy)
                {
                    return;
                }

                gameObject.SetActive(false);
            }
        }
    }
}