﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameHelper
{
    public static class UIHelper
    {
        private static readonly int MainTex = Shader.PropertyToID("_MainTex");
        private static readonly int Progress = Shader.PropertyToID("_Progress");
        
        public static async void SceneTransition(this Image img, Sprite sprTransition, float transitionSpeed = 1f,
            Action onTransitionStarted = null, Action onTransitionCompleted = null)
        {
            onTransitionStarted?.Invoke();
            img.material.SetTexture(MainTex, sprTransition.texture);

            while (img.material.GetFloat(Progress) < 1.1f)
            {
                img.material.SetFloat(Progress, Mathf.MoveTowards(img.material.GetFloat(Progress),
                    1.1f, transitionSpeed * Time.deltaTime));
                await UniTask.Yield();
            }

            onTransitionCompleted?.Invoke();
        }

        public static async void FadeOut(this Image img, float fadeOutTime = 4f, Action onFadeOutStarted = null,
            Action onFadeOutCompleted = null)
        {
            var rate = 1f / fadeOutTime;
            var progress = 0f;

            img.color = Color.white;
            onFadeOutStarted?.Invoke();

            while (progress < 1f)
            {
                img.color = Color.Lerp(Color.white, Color.clear, progress);
                progress += rate * Time.deltaTime;
                await UniTask.Yield();
            }

            img.color = Color.clear;
            onFadeOutCompleted?.Invoke();
        }

        public static async void FadeIn(this Image img, float fadeInTime = 4f, Action onFadeInStarted = null,
            Action onFadeInCompleted = null)
        {
            var rate = 1f / fadeInTime;
            var progress = 0f;

            img.color = Color.clear;
            onFadeInStarted?.Invoke();

            while (progress < 1f)
            {
                img.color = Color.Lerp(Color.clear, Color.white, progress);
                progress += rate * Time.deltaTime;
                await UniTask.Yield();
            }

            img.color = Color.white;
            onFadeInCompleted?.Invoke();
        }

        public static async void FadeOut(this SpriteRenderer sr, float fadeOutTime = 4f, Action onFadeOutStarted = null,
            Action onFadeOutCompleted = null)
        {
            var rate = 1f / fadeOutTime;
            var progress = 0f;

            sr.color = Color.white;
            onFadeOutStarted?.Invoke();

            while (progress < 1f)
            {
                sr.color = Color.Lerp(Color.white, Color.clear, progress);
                progress += rate * Time.deltaTime;
                await Task.Yield();
            }

            sr.color = Color.clear;
            onFadeOutCompleted?.Invoke();
        }

        public static async void FadeIn(this SpriteRenderer sr, float fadeInTime = 4f, Action onFadeInStarted = null,
            Action onFadeInCompleted = null)
        {
            var rate = 1f / fadeInTime;
            var progress = 0f;

            sr.color = Color.clear;
            onFadeInStarted?.Invoke();

            while (progress < 1f)
            {
                sr.color = Color.Lerp(Color.clear, Color.white, progress);
                progress += rate * Time.deltaTime;
                await Task.Yield();
            }

            sr.color = Color.white;
            onFadeInCompleted?.Invoke();
        }

        public static void OpenClose(this CanvasGroup cg)
        {
            cg.alpha = cg.alpha > 0 ? 0 : 1;
            cg.blocksRaycasts = cg.blocksRaycasts != true;
        }

        public static void SetActive(this ParticleSystem ps, bool value)
        {
            if (value)
            {
                ps.gameObject.SetActive(true);
                ps.Play();
            }
            else
            {
                var main = ps.main;
                main.stopAction = ParticleSystemStopAction.Disable;

                ps.gameObject.IgnoreRaycast();
                ps.Stop();
            }
        }

        public static void IgnoreRaycast(this GameObject go)
        {
            go.layer = LayerMask.NameToLayer("Ignore Raycast");
        }

        private static string GetHtmlFromUri(string resource)
        {
            var html = string.Empty;
            var req = (HttpWebRequest) WebRequest.Create(resource);

            try
            {
                using (var resp = (HttpWebResponse) req.GetResponse())
                {
                    var isSuccess = (int) resp.StatusCode < 299 && (int) resp.StatusCode >= 200;

                    if (isSuccess)
                    {
                        using (var reader =
                            new StreamReader(resp.GetResponseStream() ?? throw new InvalidOperationException()))
                        {
                            //We are limiting the array to 80 so we don't have
                            //to parse the entire html document feel free to 
                            //adjust (probably stay under 300)
                            var cs = new char[80];
                            reader.Read(cs, 0, cs.Length);
                            html = cs.Aggregate(html, (current, ch) => current + ch);
                        }
                    }
                }
            }
            catch
            {
                return "";
            }

            return html;
        }

        public static bool NetworkEvent(Action onDisconnect = null, Action onConnected = null)
        {
            var html = GetHtmlFromUri("http://google.com");

            if (html == "")
            {
                // No connection
                onDisconnect?.Invoke();
                return false;
            }

            if (!html.Contains("schema.org/WebPage"))
            {
                // Redirecting since the beginning of googles html contains that 
                // phrase and it was not found
            }
            else
            {
                // success
                onConnected?.Invoke();
                return true;
            }

            return false;
        }

        public static Rect GetWorldRect(this RectTransform rt, Vector3 scale = default)
        {
            var corners = new Vector3[4];
            rt.GetWorldCorners(corners);

            var bottomLeft = corners[0];
            var rect = rt.rect;

            if (scale.Equals(default))
            {
                scale = Vector3.zero;
            }

            var scaledSize = new Vector2(scale.x * rect.size.x, scale.y * rect.size.y);
            return new Rect(bottomLeft, scaledSize);
        }
    }
}