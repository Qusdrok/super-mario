using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace GameAssets.Scripts.GameHelper
{
    public class ButtonLongClick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
    {
        [SerializeField] private float holdTime = 1f;

        public UnityEvent onLongClick = new UnityEvent();

        public void OnPointerDown(PointerEventData eventData)
        {
            Invoke(nameof(OnLongPress), holdTime);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            CancelInvoke(nameof(OnLongPress));
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            CancelInvoke(nameof(OnLongPress));
        }

        private void OnLongPress()
        {
            onLongClick.Invoke();
        }
    }
}