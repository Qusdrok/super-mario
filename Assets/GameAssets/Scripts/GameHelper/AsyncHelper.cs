using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;

namespace GameAssets.Scripts.GameHelper
{
    public static class AsyncHelper
    {
        private static readonly object PropertyValueCheckLock = new object();

        public static async UniTask<T> RunToComplete<T>(Expression<Func<bool>> flag, Func<Task<T>> action,
            T defaultValue = default)
        {
            lock (PropertyValueCheckLock)
            {
                if (flag.GetPropertyValue())
                {
                    return defaultValue;
                }

                flag.SetPropertyValue(true);
            }

            try
            {
                return await action();
            }
            finally
            {
                flag.SetPropertyValue(false);
            }
        }

        public static T GetPropertyValue<T>(this Expression<Func<T>> lambda)
        {
            return lambda.Compile().Invoke();
        }

        public static TV GetPropertyValue<T, TV>(this Expression<Func<T, TV>> lambda, T input)
        {
            return lambda.Compile().Invoke(input);
        }

        public static void SetPropertyValue<T>(this Expression<Func<T>> lambda, T value)
        {
            var expression = lambda.Body as MemberExpression;
            var propertyInfo = (PropertyInfo) expression?.Member;
            var target = Expression.Lambda(expression?.Expression ?? throw new InvalidOperationException()).Compile()
                .DynamicInvoke();
            propertyInfo.SetValue(target, value);
        }

        public static void SetPropertyValue<T, TV>(this Expression<Func<T, TV>> lambda, TV value, T input)
        {
            var expression = lambda.Body as MemberExpression;
            var propertyInfo = (PropertyInfo) expression?.Member;
            propertyInfo?.SetValue(input, value);
        }
    }
}