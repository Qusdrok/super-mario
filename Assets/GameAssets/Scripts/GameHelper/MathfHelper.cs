﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.General;
using UnityEngine;
using SRandom = System.Random;
using URandom = UnityEngine.Random;

namespace GameAssets.Scripts.GameHelper
{
    public static class MathfHelper
    {
        #region Transform

        public static void SetPosition(this Transform origin, Transform target)
        {
            origin.position = target.position;
        }

        #endregion
        
        #region Void

        public static void MoveLerp(this Component t, Vector3 target, float moveSpeed, bool changeToTime = true)
        {
            t.transform.position = Vector3.Lerp(t.transform.position, target,
                moveSpeed * (changeToTime ? Time.fixedDeltaTime : 1f));
        }

        #endregion

        #region Quaternion

        public static Quaternion ToQuaternion(this Vector3 v)
        {
            var angle = Mathf.Atan2(v.x, v.z);
            return new Quaternion(v.x * Mathf.Sin(angle / 2), v.y * Mathf.Sin(angle / 2), v.z * Mathf.Sin(angle / 2),
                Mathf.Cos(angle / 2));
        }

        #endregion

        #region Vector3

        public static Vector3 Random(this Bounds bounds, float scale = 1f)
        {
            var min = bounds.min;
            var max = bounds.max;
            return new Vector3(URandom.Range(min.x * scale, max.x * scale),
                URandom.Range(min.y * scale, max.y * scale),
                URandom.Range(min.z * scale, max.z * scale));
        }

        public static Vector3 GetPlatformPosition()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.WindowsPlayer:
                    return Input.mousePosition;

                case RuntimePlatform.Android:
                case RuntimePlatform.IPhonePlayer:
                    return Input.touchCount > 0 ? (Vector3) Input.GetTouch(0).position : Vector3.zero;

                default:
                    return Vector3.zero;
            }
        }

        public static Vector3 Abs(this Vector3 v)
        {
            return new Vector3(Mathf.Abs(v.x), Mathf.Abs(v.y), Mathf.Abs(v.z));
        }

        public static Vector3 GetDirection(this Vector3 v)
        {
            var d = XYZDirection(v);

            switch (d)
            {
                case 0:
                    return Vector3.right;

                case 1:
                    return Vector3.up;

                case 2:
                    return Vector3.forward;

                default:
                    throw new InvalidOperationException();
            }
        }

        public static Vector3 GetDirectionFromInt(this int i)
        {
            switch (i)
            {
                case 0:
                    return Vector3.right;

                case 1:
                    return Vector3.up;

                case 2:
                    return Vector3.forward;

                default:
                    throw new InvalidOperationException();
            }
        }

        #endregion

        #region Int

        public static int XYZDirection(this Vector3 v)
        {
            var x = Mathf.Abs(v.x);
            var y = Mathf.Abs(v.y);
            var z = Mathf.Abs(v.z);
            return x > y & x > z ? 0 : y > x & y > z ? 1 : 2;
        }

        public static int? GetIndexOfLowestLayer(this int layer)
        {
            for (var index = 0; index < sizeof(int) * 8; index++)
            {
                if ((layer & 1) == 1)
                {
                    return index;
                }

                layer >>= 1;
            }

            return null;
        }

        #endregion

        #region Float

        public static float MaxAbs(this float a, float b)
        {
            return Mathf.Abs(a) > Mathf.Abs(b) ? a : b;
        }

        public static float NextFloat(this SRandom rnd, float max, float min)
        {
            if (min > max)
            {
                return (float) rnd.NextDouble() * (min - max) + max;
            }

            return (float) rnd.NextDouble() * (max - min) + min;
        }

        public static float RemapNumber(this float n, float l1, float h1, float l2, float h2)
        {
            return l2 + (n - l1) * (h2 - l2) / (h1 - l1);
        }

        public static float RemapNumberClamped(this float n, float l1, float h1, float l2, float h2)
        {
            return Mathf.Clamp(RemapNumber(n, l1, h1, l2, h2), Mathf.Min(l2, h2),
                Mathf.Max(l2, h2));
        }

        public static float DropChance(this List<float> drops)
        {
            var range = drops.Sum();
            var rnd = URandom.Range(0, range);
            var top = 0f;

            foreach (var x in drops)
            {
                top += x;

                if (rnd < top)
                {
                    return x;
                }
            }

            return drops[0];
        }

        #endregion

        #region Double

        public static double NextDouble(this SRandom rnd, double max, double min)
        {
            if (min > max)
            {
                return rnd.NextDouble() * (min - max) + max;
            }

            return rnd.NextDouble() * (max - min) + min;
        }

        #endregion

        #region String

        public static string Minimum10(this float value)
        {
            return value < 10 ? $"0{value}" : $"{value}";
        }

        public static string Minimum10(this int value)
        {
            return value < 10 ? $"0{value}" : $"{value}";
        }

        public static string Minimum10(this double value)
        {
            return value < 10 ? $"0{value}" : $"{value}";
        }

        public static string Match(this string s, char c1, char c2 = '\0', bool isFirstLast = false)
        {
            var begin = isFirstLast ? s.LastIndexOf(c1) : s.IndexOf(c1);
            var end = c2 == '\0' ? s.Length : s.LastIndexOf(c2);
            return s.Substring(begin + 1, end - begin - 1);
        }

        #endregion

        #region Bool

        public static bool NearlyEqual(this float a, float b, float epsilon = 1f)
        {
            var absA = Math.Abs(a);
            var absB = Math.Abs(b);
            var diff = Math.Abs(a - b);

            if (a.Equals(b))
            {
                return true;
            }

            if (a.Equals(0) || b.Equals(0) || absA + absB < Const.MIN_NORMAL)
            {
                return diff < epsilon * Const.MIN_NORMAL;
            }

            return diff / (absA + absB) < epsilon;
        }

        public static bool NextBool => URandom.value > 0.5f;

        public static bool Distance(this Component a, Component b, float minDistance)
        {
            return (a.transform.position - b.transform.position).sqrMagnitude < minDistance * minDistance;
        }

        #endregion
    }
}