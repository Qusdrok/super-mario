using System;
using GameAssets.Scripts.GameHelper;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D rb;
        [SerializeField] private float moveSpeed = 5f;

        private float _moveSpeed;
        
        private bool _isMoving;

        private void Awake()
        {
            _moveSpeed=moveSpeed;
        }

        private void FixedUpdate()
        {
            if (_isMoving)
            {
                rb.velocity = Vector2.right * (_moveSpeed * Time.fixedDeltaTime);
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            _isMoving = false;
            
            if (other.collider.CompareTag("Monster"))
            {
                var monster = other.collider.GetComponent<Monster.Monster>();
                SpawnerHelper.DestroySpawner(gameObject);
                monster.Death();
            }
            else
            {
                SpawnerHelper.DestroySpawner(gameObject);
            }
        }

        public void Shoot(bool flip)
        {
            _isMoving = true;
            _moveSpeed = moveSpeed * (flip ? 1 : -1);
        }
    }
}