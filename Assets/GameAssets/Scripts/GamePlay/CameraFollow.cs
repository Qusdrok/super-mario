﻿using System;
using DG.Tweening;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace GameAssets.Scripts.GamePlay
{
    public class CameraFollow : BaseSingleton<CameraFollow>
    {
        [SerializeField] private ParticleSystem confettiFx;
        [SerializeField] private Transform player;
        [Range(0f, 1f)] [SerializeField] private float lerpSpeed = 0.25f;

        private Vector3 _originOffset;
        private Tilemap _tilemap;

        private float _xMin;
        private float _xMax;

        private float _yMin;
        private float _yMax;

        private void Start()
        {
            if (player != null)
            {
                _originOffset = transform.position - player.position;
            }

            _tilemap = FindObjectOfType<Tilemap>();
            SetLimit();
        }

        protected override void InnerLateUpdate()
        {
            if (player == null)
            {
                return;
            }

            var position = player.position;
            var target = new Vector3(Mathf.Clamp(position.x, _xMin, _xMax),
                Mathf.Clamp(position.y, _yMin, _yMax), -10);
            transform.position = Vector3.Lerp(transform.position, target, lerpSpeed);
        }

        private void SetLimit()
        {
            _tilemap.CompressBounds();

            var min = _tilemap.CellToWorld(_tilemap.cellBounds.min);
            var max = _tilemap.CellToWorld(_tilemap.cellBounds.max);

            var height = 2f * GameManager.GameManager.Instance.Camera.orthographicSize;
            var width = height * GameManager.GameManager.Instance.Camera.aspect;

            _xMin = min.x + width / 2;
            _xMax = max.x - width / 2;

            _yMin = min.y + height / 2;
            _yMax = max.y - height / 2;
        }

        public void EnableConfetti()
        {
            if (confettiFx == null)
            {
                return;
            }

            confettiFx.SetActive(true);
        }
    }
}