using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Monster.Crab
{
    public class CrabMonster : Monster
    {
        [SerializeField] protected LayerMask groundLayerMask;
        
        private readonly Vector3 _size = new Vector3(0.01f, 0.05f, 0f);

        protected override void Move()
        {
            base.Move();
            var p = down.position;
            var box = Physics2D.OverlapBox(p, _size, transform.eulerAngles.z, groundLayerMask);

            if (box == null)
            {
                transform.RotateAround(p, new Vector3(0, 0, -Mathf.Sign(stat.moveSpeed)), 90);
            }
        }
    }
}