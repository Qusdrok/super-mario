using System;
using Cysharp.Threading.Tasks;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameInterface;
using GameAssets.Scripts.GameManager;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Monster
{
    public partial class Monster : IStatus
    {
        public void Hurt()
        {
            Death();
        }

        public void HurtBullet()
        {
            Death();
        }

        public async void Death()
        {
            gameObject.IgnoreRaycast();
            UIManager.Instance.AddScore(transform, score);
            bcAnimator.ChangeAnimation(AnimationType.Death);
            stat.ChangeCharacterState(CharacterState.Death);
            rb.velocity = new Vector2(Mathf.Sign(down.position.x - transform.position.x) * flySpeed, 2f);

            await UniTask.Delay(TimeSpan.FromSeconds(2f));
            gameObject.SetActive(false);
        }
    }
}