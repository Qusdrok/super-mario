using GameAssets.Scripts.GameBase;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Monster
{
    public partial class Monster : BaseCharacter
    {
        [SerializeField] private new SpriteRenderer renderer;
        [SerializeField] protected Transform down;

        [SerializeField] private int score;

        [ShowIf(nameof(isClamp))] [SerializeField]
        private float flySpeed = 0.5f;

        [ShowIf(nameof(isClamp))] [SerializeField]
        private float minX;

        [ShowIf(nameof(isClamp))] [SerializeField]
        private float maxX;

        [SerializeField] private bool isClamp = true;

        protected virtual void Move()
        {
            transform.Translate(Vector3.right * (stat.moveSpeed * Time.fixedDeltaTime));

            if (isClamp)
            {
                var p = transform.position;
                p = new Vector3(Mathf.Clamp(p.x, minX, maxX), p.y, p.z);
                transform.position = p;
            }

            if (transform.position.x <= minX && stat.moveSpeed < 0 ||
                transform.position.x >= maxX && stat.moveSpeed > 0)
            {
                Flip();
            }
        }

        private void Flip()
        {
            stat.moveSpeed *= -1;
            renderer.flipX = stat.moveSpeed > 0;
        }
    }
}