using System;
using Cysharp.Threading.Tasks;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameInterface;
using GameAssets.Scripts.General;

namespace GameAssets.Scripts.GamePlay.Player
{
    public partial class Player : IStatus
    {
        public async void Hurt()
        {
            if (_isHurt)
            {
                return;
            }

            _isHurt = true;
            hearts[(int) stat.AddHp(-1)].sprite = heartEmpty;

            for (var i = 0; i < 40; i++)
            {
                FlashColor(i % 2 == 0);
                await UniTask.Delay(TimeSpan.FromSeconds(0.05f));
            }

            _isHurt = false;
        }

        public void HurtBullet()
        {
        }

        public void Death()
        {
            if (stat.hp <= 0)
            {
                GameManager.GameManager.Instance.OnLose();
            }
            else
            {
                bcAnimator.ChangeAnimation(AnimationType.Falling);
                transform.position = _checkPoint;
                rb.ResetInertia();
                Hurt();
            }
        }
    }
}