using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameManager;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Player
{
    public partial class Player
    {
        private void Awake()
        {
            _mtBlock = new MaterialPropertyBlock();
            _renderer = GetComponent<Renderer>();
            _checkPoint = transform.position;
            _fillPhase = Shader.PropertyToID("_Black");
            bcAnimator.ChangeAnimation(AnimationType.Falling);

            GameEvent.GameEvent.OnPlayerShoot += SingleShoot;
            GameEvent.GameEvent.OnPlayerJump += DoJump;
        }

        private void OnDestroy()
        {
            GameEvent.GameEvent.OnPlayerShoot -= SingleShoot;
            GameEvent.GameEvent.OnPlayerJump -= DoJump;
        }

        protected override void InnerUpdate()
        {
            base.InnerUpdate();

            if (!_renderer.isVisible)
            {
                Death();
                return;
            }

            if (stat.IsCharacterState(CharacterState.Move))
            {
                rb.velocity = _direction * (stat.moveSpeed * Time.fixedDeltaTime);
            }

            var size = Physics2D.RaycastNonAlloc(transform.position, Vector2.down * distance, _hit2Ds, distance);

            if (size <= 0)
            {
                return;
            }

            for (var i = 0; i < size; i++)
            {
                if (!_hit2Ds[i].collider.CompareTag("Ground") || _isJump ||
                    stat.IsCharacterState(CharacterState.Move))
                {
                    continue;
                }

                stat.IsCharacterState(CharacterState.IdleOrRespawn);
                bcAnimator.ChangeAnimation(AnimationType.Idle);

                rb.ResetInertia();
                _isJump = true;
                _isFalling = false;
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            switch (other.collider.tag)
            {
                case "Monster":
                    var monster = other.collider.GetComponent<Monster.Monster>();

                    if (!monster.stat.IsAlive)
                    {
                        return;
                    }

                    if (transform.position.y < other.collider.transform.position.y)
                    {
                        Hurt();
                    }
                    else
                    {
                        monster.Hurt();
                    }

                    break;

                case "CheckPoint":
                    CombatManager.Instance.CreateToast(other.collider.transform, "Check point");
                    _checkPoint = other.collider.transform.position;
                    other.collider.gameObject.SetActive(false);
                    break;

                case "Coin":
                    UIManager.Instance.AddScore(other.collider.transform, 100);
                    other.collider.gameObject.SetActive(false);
                    break;

                case "BoxGold":
                    if (transform.position.y < other.collider.transform.position.y)
                    {
                        other.collider.GetComponent<BoxGold>()?.Break();
                    }

                    break;

                case "Castle":
                    if (stat.IsAlive)
                    {
                        GameManager.GameManager.Instance.OnWin();
                    }

                    break;
            }
        }
    }
}