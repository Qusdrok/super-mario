﻿using System.Collections.Generic;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameManager;
using GameAssets.Scripts.General;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GamePlay.Player
{
    public partial class Player : BaseCharacter
    {
        [SerializeField] private List<Image> hearts = new List<Image>();
        [SerializeField] private Sprite heartEmpty;

        [SerializeField] private GameObject bulletPrefab;
        [SerializeField] private Transform exitPoint;
        [SerializeField] private Color fillPhaseColor;

        [SerializeField] private float distance;
        [SerializeField] private float thrustForce;

        private readonly RaycastHit2D[] _hit2Ds = new RaycastHit2D[10];
        private MaterialPropertyBlock _mtBlock;
        private Renderer _renderer;
        private Vector3 _checkPoint;
        private Vector3 _direction;

        private int _fillPhase;

        private bool _isJump;
        private bool _isFalling = true;
        private bool _isHurt;

        private void FlashColor(bool change)
        {
            _mtBlock.SetColor(_fillPhase, change ? Color.red : fillPhaseColor);
            _renderer.SetPropertyBlock(_mtBlock);
        }

        private void SingleShoot()
        {
            if (!GameManager.GameManager.Instance.IsGameState(GameState.Playing))
            {
                return;
            }
            
            UIManager.Instance.CanShoot(() =>
            {
                var bullet = SpawnerHelper.CreateSpawner(exitPoint.position, null, bulletPrefab);
                bullet.GetComponent<Bullet>().Shoot(transform.localScale.x >= 0f);
            });
        }

        private void DoJump()
        {
            if (!_isJump || !GameManager.GameManager.Instance.IsGameState(GameState.Playing))
            {
                return;
            }

            _isJump = false;
            bcAnimator.ChangeAnimation(AnimationType.Jump);
            rb.AddForce(Vector2.up * thrustForce, ForceMode2D.Force);
            bcAnimator.ChangeAnimation(AnimationType.Falling);
            _isFalling = true;
        }

        public void DoMoveLeft()
        {
            if (!GameManager.GameManager.Instance.IsGameState(GameState.Playing))
            {
                return;
            }

            var scale = transform.localScale;

            if (scale.x > 0f)
            {
                scale.x *= -1;
            }

            stat.SetCharacterState(CharacterState.Move);
            bcAnimator.ChangeAnimation(AnimationType.Move);

            _direction = Vector3.left;
            transform.localScale = scale;
        }

        public void DoMoveRight()
        {
            if (!GameManager.GameManager.Instance.IsGameState(GameState.Playing))
            {
                return;
            }

            var scale = transform.localScale;

            if (scale.x < 0f)
            {
                scale.x *= -1;
            }

            stat.SetCharacterState(CharacterState.Move);
            bcAnimator.ChangeAnimation(AnimationType.Move);

            _direction = Vector3.right;
            transform.localScale = scale;
        }

        public void DoStopMove()
        {
            if (!GameManager.GameManager.Instance.IsGameState(GameState.Playing))
            {
                return;
            }

            rb.ResetInertia();
            stat.SetCharacterState(CharacterState.IdleOrRespawn);
            bcAnimator.ChangeAnimation(AnimationType.Idle);
        }
    }
}