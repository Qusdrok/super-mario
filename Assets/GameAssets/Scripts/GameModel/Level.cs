﻿using UnityEngine;

namespace GameAssets.Scripts.GameModel
{
    [CreateAssetMenu(fileName = "Level", menuName = "UI/Level", order = 0)]
    public class Level : ScriptableObject
    {
        public GameObject levelPrefab;
        public int winMoney;
    }
}