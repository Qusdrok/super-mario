﻿using GameAssets.Scripts.GameManager;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameModel
{
    [CreateAssetMenu(fileName = "Audio Asset", menuName = "UI/Audio Asset", order = 0)]
    public class AudioAsset : ScriptableObject
    {
        [Header("Sound")] [SerializeField] private Sprite uiSoundOn;
        [SerializeField] private Sprite uiSoundOff;

        [Header("Music")] [SerializeField] private Sprite uiMusicOn;
        [SerializeField] private Sprite uiMusicOff;

        [Header("Vibration")] [SerializeField] private Sprite uiVibrationOn;
        [SerializeField] private Sprite uiVibrationOff;

        private UserDataManager _udm;

        private void Awake()
        {
            _udm = UserDataManager.Instance;
        }

        public void SetMusic(Image imgMusic, bool change = false)
        {
            if (!change)
            {
                _udm.SetMusic(!_udm.userDataSave.music);
            }

            imgMusic.sprite = _udm.userDataSave.music ? uiMusicOn : uiMusicOff;
        }

        public void SetSound(Image imgSound, bool change = false)
        {
            if (!change)
            {
                _udm.SetSound(!_udm.userDataSave.sound);
            }

            imgSound.sprite = _udm.userDataSave.sound ? uiSoundOn : uiSoundOff;
        }

        public void SetVibration(Image imgVibration, bool change = false)
        {
            if (!change)
            {
                _udm.SetVibration(!_udm.userDataSave.vibration);
            }

            imgVibration.sprite = _udm.userDataSave.vibration ? uiVibrationOn : uiVibrationOff;
        }
    }
}