﻿using System;

namespace GameAssets.Scripts.GameModel
{
    [Serializable]
    public class Highscore
    {
        public string name;
        public string timer;
        public string deaths;
        public string floor;

        public Highscore(string name, string timer, string deaths, string floor)
        {
            this.name = name;
            this.timer = timer;
            this.deaths = deaths;
            this.floor = floor;
        }
    }
}