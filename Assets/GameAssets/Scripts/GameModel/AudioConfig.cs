﻿using UnityEngine;

namespace GameAssets.Scripts.GameModel
{
    [CreateAssetMenu(fileName = "Audio Config", menuName = "UI/Audio Config", order = 0)]
    public class AudioConfig : ScriptableObject
    {
        public AudioClip clipSound;
        public string clipName;
    }
}