namespace GameAssets.Scripts.GameInterface
{
    public interface IStatus
    {
        void Hurt();
        void HurtBullet();
        void Death();
    }
}