﻿using UnityEngine;

namespace GameAssets.Scripts.GameEvent
{
    public class GameEvent : MonoBehaviour
    {
        #region Events

        // Main events
        public delegate void TestWin();

        public delegate void TestLose();

        public event TestWin OnTestWin;
        public event TestLose OnTestLose;

        // Game events
        public delegate void PlayerShoot();

        public delegate void PlayerJump();

        public delegate void PlayerMoveLeft();

        public delegate void PlayerMoveRight();

        public static event PlayerShoot OnPlayerShoot;
        public static event PlayerJump OnPlayerJump;

        #endregion

        #region Main events

        private void Awake()
        {
            OnTestWin += TestWinHandle;
            OnTestLose += TestLoseHandle;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                OnTestWin?.Invoke();
            }

            if (Input.GetKeyDown(KeyCode.L))
            {
                OnTestLose?.Invoke();
            }
        }

        private void OnDestroy()
        {
            OnTestWin -= TestWinHandle;
            OnTestLose -= TestLoseHandle;
        }

        private static void TestWinHandle()
        {
            GameManager.GameManager.Instance.OnWin();
        }

        private static void TestLoseHandle()
        {
            GameManager.GameManager.Instance.OnLose();
        }

        #endregion

        public static void DoPlayerShoot() => OnPlayerShoot?.Invoke();
        public static void DoPlayerJump() => OnPlayerJump?.Invoke();
    }
}