﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameModel;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameManager
{
    public class AudioManager : BaseSingleton<AudioManager>
    {
        [SerializeField] private List<AudioConfig> audioClips = new List<AudioConfig>();

        [Header("Audio Asset")] [SerializeField]
        private AudioAsset audioAsset;

        [Header("Audio Source")] [SerializeField]
        private AudioSource audioSound;

        [SerializeField] private AudioSource audioMusic;

        [Header("Sound")] [SerializeField] private Button btnSound;
        [SerializeField] private Image imgSound;

        [Header("Music")] [SerializeField] private Button btnMusic;
        [SerializeField] private Image imgMusic;

        [Header("Vibration")] [SerializeField] private Button btnVibration;
        [SerializeField] private Image imgVibration;

        private int _loop;

        protected override void Awake()
        {
            base.Awake();
            OnEnableAudio();

            if (btnSound != null)
            {
                btnSound.onClick.AddListener(() =>
                {
                    audioAsset.SetSound(imgSound, true);
                    OnUpdateVolume();
                });
            }

            if (btnMusic != null)
            {
                btnMusic.onClick.AddListener(() =>
                {
                    audioAsset.SetSound(imgMusic, true);
                    OnUpdateVolume();
                });
            }

            if (btnVibration != null)
            {
                btnVibration.onClick.AddListener(() => audioAsset.SetVibration(imgVibration, true));
            }
        }

        private void OnEnableAudio()
        {
            OnUpdateVolume();

            if (audioMusic.volume >= 1)
            {
                PlayMusic("Background");
            }
            else
            {
                StopMusic();
            }
        }

        private void OnUpdateVolume()
        {
            audioSound.volume = UserDataManager.Instance.userDataSave.sound ? 1 : 0;
            audioMusic.volume = UserDataManager.Instance.userDataSave.music ? 1 : 0;
        }

        private void PlayMusic(string clipName)
        {
            var clip = GetClip(clipName);

            if (clip == null)
            {
                return;
            }

            audioMusic.clip = clip;
            audioMusic.Play();
        }

        public void StopMusic()
        {
            if (audioMusic.isPlaying)
            {
                audioMusic.Stop();
            }
        }

        public void PlaySoundLoop(string clipName, int loopTimes = 1)
        {
            var clip = GetClip(clipName);

            if (loopTimes < 2)
            {
                audioSound.PlayOneShot(clip);
            }
            else
            {
                _loop = 0;
                audioSound.clip = clip;
                StartCoroutine(Loop(clip, loopTimes));
            }
        }

        public void StopSound()
        {
            if (audioSound.isPlaying)
            {
                audioSound.Stop();
            }
        }

        public void PlayVibrate()
        {
            if (UserDataManager.Instance.userDataSave.vibration)
            {
                AndroidVibration.Vibrate();
            }
        }

        private AudioClip GetClip(string soundName)
        {
            return (from x in audioClips where x.clipName.Equals(soundName) select x.clipSound).FirstOrDefault();
        }

        private IEnumerator Loop(AudioClip clip, int loop)
        {
            _loop += 1;
            audioSound.Play();

            yield return new WaitForSeconds(clip.length / 2);

            if (_loop < loop)
            {
                StartCoroutine(Loop(clip, loop));
            }
        }
    }
}